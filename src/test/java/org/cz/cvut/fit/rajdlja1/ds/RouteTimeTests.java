package org.cz.cvut.fit.rajdlja1.ds;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.cz.cvut.fit.rajdlja1.ds.domain.Depot;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;

import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * Created by JR on 4/5/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class RouteTimeTests {


    private SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd-MM-yyyy h:m");

    @Before
    public void before() throws Exception {
        // define a fixed date-time
        Date fixedDateTime = DATE_TIME_FORMATTER.parse("02-04-2018 9:00");
        DateTimeUtils.setCurrentMillisFixed(fixedDateTime.getTime());
    }

    @After
    public void after() throws Exception {
        // Make sure to cleanup afterwards
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    public void isOnTheWayTest() throws JsonProcessingException, ParseException {

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 9:30");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");

        Depot d = new Depot(1,"Praha");
        Depot d2 = new Depot(2,"Brno");

        Route r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertTrue(r.onTheWayBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 8:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 10:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.onTheWayBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 10:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 11:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.onTheWayBetween(since, until));
    }

    @Test
    public void isArrivelTimeBetweenTest() throws JsonProcessingException, ParseException {

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 9:30");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");

        Depot d = new Depot(1,"Praha");
        Depot d2 = new Depot(2,"Brno");

        Route r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertTrue(r.arrivalBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 8:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 10:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.arrivalBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 11:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 12:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.arrivalBetween(since, until));
    }

    @Test
    public void isDepartureTimeBetweenTest() throws JsonProcessingException, ParseException {
        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 8:00");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:00");

        Depot d = new Depot(1,"Praha");
        Depot d2 = new Depot(2,"Brno");

        Route r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertTrue(r.departureBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 6:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 8:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.departureBetween(since, until));

        timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");
        since = DATE_TIME_FORMATTER.parse("02-04-2018 9:00");
        until = DATE_TIME_FORMATTER.parse("02-04-2018 11:00");
        r = new Route(1, null, d, d2, timeOfDeparture, timeOfArrival);
        assertFalse(r.departureBetween(since, until));
    }

}