package org.cz.cvut.fit.rajdlja1.ds;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cz.cvut.fit.rajdlja1.ds.domain.Depot;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.domain.TrainSet;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * Created by JR on 4/5/2018.
 */
public class JsonSerializationTests {

    private SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd-MM-yyyy h:m");

    /*@Test
    public void givenBidirectionRelationTrainSetRouteTest() throws JsonProcessingException, ParseException {

        TrainSet ts = new TrainSet(1, "Souprava A");

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("01-04-2018 8:30");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("01-04-2018 10:00");

        Depot d = new Depot(1,"Praha");
        Depot d2 = new Depot(2,"Brno");

        Route r = new Route(1, d, d2, timeOfDeparture, timeOfArrival);

        r.setTrainSet(ts);
        ts.addRoute(r);

        String result = new ObjectMapper().writeValueAsString(r);

        assertThat(result, containsString("id"));
        assertThat(result, containsString("from"));
        assertThat(result, containsString("to"));
        assertThat(result, containsString("departureTime"));
        assertThat(result, containsString("arrivalTime"));
        assertThat(result, containsString("trainSet"));

        result = new ObjectMapper().writeValueAsString(ts);
        assertThat(result, containsString("id"));
        assertThat(result, containsString("name"));
        assertThat(result, containsString("route"));
    }*/
}
