package org.cz.cvut.fit.rajdlja1.ds;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.DepotMockedRepository;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by JR on 4/5/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class VehicleAvailabilityTests {


    private SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd-MM-yyyy h:m");


    @Before
    public void before() throws Exception {
        // define a fixed date-time
        Date fixedDateTime = DATE_TIME_FORMATTER.parse("02-04-2018 9:00");
        DateTimeUtils.setCurrentMillisFixed(fixedDateTime.getTime());
    }

    @After
    public void after() throws Exception {
        // Make sure to cleanup afterwards
        DateTimeUtils.setCurrentMillisSystem();
    }

    @Test
    // Car and Locomotive are available in the depot in the given interval
    public void findAvailableVehicles() throws JsonProcessingException, ParseException, ChangeSetPersister.NotFoundException {
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("01-01-1999 " + "23:58");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("01-01-1999 " + "23:59");

        List<Vehicle> availableVehicles = prepareTestData(since, until, timeOfDeparture, timeOfArrival);

        assertThat( availableVehicles, containsInAnyOrder(
                hasProperty("name", is("OS-18:000")),
                hasProperty("name", is("LOK-18:001"))
        ));
    }

    @Test
    // Car and Locomotive are not available in the depot in the given interval
    // Car and Locomotive has not been initialized - has not any ride planned before the given interval
    public void findAvailableVehicles2() throws JsonProcessingException, ParseException, ChangeSetPersister.NotFoundException {
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 11:00");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 13:00");

        List<Vehicle> availableVehicles = prepareTestData(since, until, timeOfDeparture, timeOfArrival);

        assertThat(availableVehicles, hasSize(0));
    }

    @Test
    // Car and Locomotive are on the way during the given interval - vehicles are not available in the depot
    public void findAvailableVehicles3() throws JsonProcessingException, ParseException, ChangeSetPersister.NotFoundException {
        Date since = DATE_TIME_FORMATTER.parse("02-04-2018 8:30");
        Date until = DATE_TIME_FORMATTER.parse("02-04-2018 10:30");

        Date timeOfDeparture = DATE_TIME_FORMATTER.parse("02-04-2018 7:00");
        Date timeOfArrival = DATE_TIME_FORMATTER.parse("02-04-2018 9:00");

        List<Vehicle> availableVehicles = prepareTestData(since, until, timeOfDeparture, timeOfArrival);

        assertThat(availableVehicles, hasSize(0));
    }

    public List<Vehicle> prepareTestData(Date since, Date until, Date timeOfDeparture, Date timeOfArrival) throws ParseException, ChangeSetPersister.NotFoundException {
        List<Vehicle> testVehicleList = new ArrayList<>();

        testVehicleList.add(new Car(0, "OS-18:000", "Osobní vůz"));
        testVehicleList.add(new Locomotive(1, "LOK-18:001", "El. lokomotiva"));

        Depot d = new Depot(0,"Praha");
        Depot d2 = new Depot(1,"Brno");

        Route route = new Route(0, "R000000", d, d2, timeOfDeparture, timeOfArrival);
        RouteSection rs = new RouteSection(0, route, d, d2, timeOfDeparture, timeOfArrival);
        route.addRouteSection(rs);
        Train t = new Train(0, timeOfDeparture, timeOfArrival, route);
        Ride ride = new Ride(0, null, null, t, rs);
        ride.setVehicles(testVehicleList);

        for(Vehicle vehicle : testVehicleList) {
            vehicle.addRide(ride);
        }

        DepotMockedRepository repository = new DepotMockedRepository();
        return repository.findAvailableVehicles(1, since, until, testVehicleList);
    }


}