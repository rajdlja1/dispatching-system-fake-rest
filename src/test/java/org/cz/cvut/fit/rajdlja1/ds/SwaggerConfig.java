package org.cz.cvut.fit.rajdlja1.ds;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;


/**
 * Created by JR on 3/23/2018.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()                 .apis(RequestHandlerSelectors.basePackage("org.cz.cvut.fit.rajdlja1.ds"))
                .paths(regex(".*"))
                .build()
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Fake REST API for dispatching system",
                "Spring Boot API simulující backendovou část aplikace dispečingového systému vlakové dopravní společnosti pro vývojářské a testovací účely.",
                "1.0",
                "Terms of service",
                new Contact("Jan Rajdl", "https://rajdl.com/about", "rajdlja1@fit.cvut.cz"),
                "Open-source License",
                "https://opensource.org");
        return apiInfo;
    }

}