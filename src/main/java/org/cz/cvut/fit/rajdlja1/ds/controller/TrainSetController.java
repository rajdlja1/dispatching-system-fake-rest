package org.cz.cvut.fit.rajdlja1.ds.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.*;
import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;
import org.cz.cvut.fit.rajdlja1.ds.exception.BadFormatException;
import org.cz.cvut.fit.rajdlja1.ds.service.TrainSetService;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 3/19/2018.
 */

@RestController
@RequestMapping("/soupravy")
@Api(description="Operace týkající se administrace vlakových souprav v rámci dispečingového systému.")
public class TrainSetController {

    private static final String TRAIN_SET_BAD_FORMAT = "Vlaková souprava neodpovídá požadovanému formátu.";

    @Autowired
    private TrainSetService service;

    @JsonView(CustomView.TrainsetView.class)
    @RequestMapping(path="/hledej", method= RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí seznam všech souprav pro zvolený časový interval.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácený seznam vlakových souprav pro interval. " +
                    "Pokud není vyplněn některý z časových údajů od, do, je tento údaj nahrazen aktuálním datem s časem 0:00 pro od a 23:59 pro do." +
                    "Pokud není vyplněn parametr status, defaultně se vyhledávají soupravy, které jsou aktuálně na cestě.") })
    public Iterable<TrainSet> getTrainSets( @RequestParam(value = "od", required = false) String strSince,
                                            @RequestParam(value = "do", required = false) String strUntil,
                                            @RequestParam(value = "cas", required = false) String strStatus
    ) throws BadFormatException {
        List<Date> dates = ControllerValidation.validateDateFromUrl(strSince,strUntil);

        if (strStatus != null) {
            return service.findAll(dates.get(0), dates.get(1), ControllerValidation.validateStatusParameterFromUrl(strStatus));
        }

        return service.findAll(dates.get(0), dates.get(1), StatusEnum.ON_THE_WAY);
    }

    @JsonView(CustomView.TrainsetDetailView.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí vlakovou soupravu podle identifikátoru soupravy.", response = TrainSet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácený objekt vlakové soupravy."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    public TrainSet getTrainSet(@ApiParam(value = "ID vlakové soupravy", required = true) @PathVariable("id") Integer id) throws NotFoundException {
        return service.findOne(id);
    }

    @JsonView(CustomView.TrainsetDetailView.class)
    @RequestMapping(value = "/{id}/hledej", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí vlakovou soupravu podle identifikátoru soupravy s jejími vlaky v daném časovém intervalu.", response = TrainSet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácený objekt vlakové soupravy."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    public TrainSet getFilteredTrainSet(@ApiParam(value = "ID vlakové soupravy", required = true) @PathVariable("id") Integer id,
                                        @RequestParam(value = "od", required = false) String strSince,
                                        @RequestParam(value = "do", required = false) String strUntil,
                                        @RequestParam(value = "cas", required = false) String strStatus) throws NotFoundException, BadFormatException {
            List<Date> dates = ControllerValidation.validateDateFromUrl(strSince,strUntil);

            if (strStatus != null) {
                return service.findFilteredOne(id, dates.get(0), dates.get(1), ControllerValidation.validateStatusParameterFromUrl(strStatus));
            }

            return service.findFilteredOne(id, dates.get(0), dates.get(1), StatusEnum.ON_THE_WAY);
    }

    @RequestMapping(value = "/{id}/spoje", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí spoje vlakové soupravy.", response = TrainSet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácené spoje vlakové soupravy."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    public Iterable<Train> getRoutesOfTrainSet(@ApiParam(value = "ID vlakové soupravy", required = true) @PathVariable("id") Integer id) throws NotFoundException {
        return service.findTrains(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Smaže vlakovou soupravu se zadaným identifikátorem.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Vlaková souprava byla úspěšně smazána."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    public void deleteTrainSet(@PathVariable("id") Integer id) throws NotFoundException {
        service.delete(id);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Vytvoří vlakovou soupravu a vrátí vytvořený objekt v json formátu.", response = TrainSet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Úspěšně uložená vlaková souprava."),
            @ApiResponse(code = 409, message = "Vlaková souprava s tímto názvem již existuje."),
            @ApiResponse(code = 400, message = "Vkládaný objekt není ve správném formátu.")
    })
    @ResponseBody
    public TrainSet createTrainSet(@RequestBody @Validated TrainSet trainSet) throws BadFormatException, DataIntegrityViolationException {
        if (trainSet == null) throw new BadFormatException(TRAIN_SET_BAD_FORMAT);
        if (trainSet.getId() != null) throw new BadFormatException(TRAIN_SET_BAD_FORMAT);
        if (trainSet.getName() ==  null) throw new BadFormatException(TRAIN_SET_BAD_FORMAT);
        return service.save(trainSet);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "Upraví existující záznam vlakové soupravy podle předaného identifikátoru.", response = TrainSet.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Úspěšně uložené změny vlakové soupravy."),
            @ApiResponse(code = 409, message = "Vlaková souprava s tímto názvem již existuje."),
            @ApiResponse(code = 400, message = "Objekt představující vlakovou soupravu není ve správném formátu."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    @ResponseBody
    public TrainSet updateTrainSet(@PathVariable("id") Integer id, @RequestBody TrainSet trainSet) throws BadFormatException, NotFoundException {
        if (trainSet == null) throw new BadFormatException(TRAIN_SET_BAD_FORMAT);
        if (id == null) throw new BadFormatException(TRAIN_SET_BAD_FORMAT);
        if (trainSet.getName() ==  null) new BadFormatException(TRAIN_SET_BAD_FORMAT);
        return service.update(trainSet);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadFormatException.class)
    public ResponseEntity<ErrorInfo> rulesForBadRequestException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(), e.getMessage());
        return new ResponseEntity<>(errInfo, responseStatus);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorInfo> rulesForViolatedConstraintException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.CONFLICT;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(), e.getMessage());
        return new ResponseEntity<>(errInfo, responseStatus);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> rulesForNotFoundException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.NOT_FOUND;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Vlaková souprava se zadaným id neexistuje.");
        return new ResponseEntity<>(errInfo, responseStatus);
    }

    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<ErrorInfo> rulesForTypeMismatchException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Špatný formát id soupravy v url adrese.");
        return new ResponseEntity<>(errInfo, responseStatus);
    }
}
