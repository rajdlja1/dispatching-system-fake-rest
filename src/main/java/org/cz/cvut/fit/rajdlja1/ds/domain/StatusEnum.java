package org.cz.cvut.fit.rajdlja1.ds.domain;

/**
 * Created by JR on 4/6/2018.
 */
public enum StatusEnum {
    ARRIVAL("PRIJEZD"),
    ON_THE_WAY("NACESTE"),
    DEPARTURE("ODJEZD");

    private String csTranslation;

    private StatusEnum(String csTranslation) {
        this.csTranslation = csTranslation;
    }

    public String getCsTranslation() {
        return csTranslation;
    }
}
