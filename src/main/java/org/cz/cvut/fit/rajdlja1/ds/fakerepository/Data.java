package org.cz.cvut.fit.rajdlja1.ds.fakerepository;

import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by JR on 4/5/2018.
 */
@Repository
public class Data {

    private SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("dd-MM-yyyy h:m");
    private SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("d-MM-yyyy");

    static Map<Integer, TrainSet> trainSets;
    static Map<Integer, Train> trains;
    static Map<Integer, Car> passengerCars;
    static Map<Integer, Car> diningCars;
    static Map<Integer, Locomotive> locomotives;
    static Map<Integer, Driver> drivers;
    static Map<Integer, Depot> depots;
    static Map<Integer, Route> routes;
    static Map<Integer, RouteSection> routeSections;
    static Map<Integer, Ride> rides;

    public static final int NUMBER_OF_TRAINSETS = 6;
    public static final int NUMBER_OF_PASSENGER_CARS = 20;
    public static final int NUMBER_OF_DINING_CARS = 8;
    public static final int NUMBER_OF_LOCOMOTIVES = 8;
    public static final int NUMBER_OF_ROUTES = 6;
    public static final int NUMBER_OF_TRAINS = 3;


    /* Creation of Depots*/
    {
        depots = new HashMap<>();
        Depot d = new Depot(0,"Praha");
        Depot d2 = new Depot(1,"Brno");
        depots.put(d.getId(), d);
        depots.put(d2.getId(), d2);
    }


    /* Creation of Routes */
    {
        routes = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_ROUTES; i++) {
            try {
                Date timeOfDeparture =  DATE_TIME_FORMATTER.parse("01-04-2018 08:30" );
                Date timeOfArrival =  DATE_TIME_FORMATTER.parse("01-04-2018 10:30" );
                Route r = new Route(2*i, "R1001"+i, depots.get(0), depots.get(1), timeOfDeparture, timeOfArrival);
                Route r2 = new Route(2*i+1, "R1002"+i, depots.get(1), depots.get(0), timeOfDeparture, timeOfArrival);
                routes.put(r.getId(),r);
                routes.put(r2.getId(),r2);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /* Creation of RouteSections */
    {
        routeSections = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_ROUTES; i++) {
            try {
                Date timeOfDeparture =  DATE_TIME_FORMATTER.parse("01-04-2018 " + "08:30" );
                Date timeOfArrival =  DATE_TIME_FORMATTER.parse("01-04-2018 " + "10:30" );
                Date timeOfDeparture2 =  DATE_TIME_FORMATTER.parse("01-04-2018 " + "08:30" );
                Date timeOfArrival2 =  DATE_TIME_FORMATTER.parse("01-04-2018 " + "10:30" );

                Route route = routes.get(i);

                RouteSection rs = new RouteSection(RouteSection.nextId(), route, depots.get(0), depots.get(1), timeOfDeparture, timeOfArrival);
                RouteSection rs2 = new RouteSection(RouteSection.nextId(), route, depots.get(0), depots.get(1), timeOfDeparture, timeOfArrival);
                route.addRouteSection(rs);
                route.addRouteSection(rs2);
                routeSections.put(rs2.getId(), rs2);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /* Creation of Vehicles */
    {
        /* Creation of Cars */
        passengerCars = new HashMap<>();
        diningCars = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_PASSENGER_CARS; i++) {
            passengerCars.put(i, new Car(Car.nextId(), "OS-18:000" + i, "Osobní vůz"));
        }

        for (int i = 0; i < NUMBER_OF_DINING_CARS; i++) {
            diningCars.put(i, new Car(Car.nextId(), "JI-18:000" + i, "Jídelní vůz"));
        }

        /* Creation of Locomotives */
        locomotives = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_LOCOMOTIVES; i++) {
            locomotives.put(i, new Locomotive(i, "LOK-18:000" + i, "El. lokomotiva"));
        }

    }

    /* Creation of Drivers*/
    {
        drivers = new HashMap<>();
        drivers.put(0, new Driver(0, "Jan", "Ridic", "+420 777 123 456", "dummy@vlaky.cz"));
        drivers.put(1, new Driver(1, "Jiří", "Strojvůdce", "+420 723 123 456", "dummy@vlaky.cz"));
        drivers.put(2, new Driver(2, "Anna", "Rychlá", "+420 777 123 456", "dummy@vlaky.cz"));
        drivers.put(3, new Driver(3, "Pavel", "Rychlík", "+421 727 123 456", "dummy@vlaky.cz"));
        drivers.put(4, new Driver(4, "Petr", "Opozdilec", "+420 775 123 456", "dummy@vlaky.cz"));
        drivers.put(5, new Driver(5, "Ondřej", "Výhybka", "+420 773 123 456", "dummy@vlaky.cz"));
        drivers.put(6, new Driver(6, "Barbora", "Přesná", "+420 779 123 456", "dummy@vlaky.cz"));
    }

    /* Creation of TrainSets */
    {
        trainSets = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_TRAINSETS; i++) {
            TrainSet ts = new TrainSet(i, "" + (char) ('A' + i));
            ts.addVehicle(locomotives.get(i));
            ts.addVehicle(diningCars.get(i));
            for (int j = i*4; j < i+4; j++) {
                ts.addVehicle(passengerCars.get(j));
            }
            trainSets.put(i, ts);
        }
    }


    /* Creation of Trains */
    {
        trains = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_TRAINS; i++) {
            try {
                Date timeOfDeparture =  DATE_TIME_FORMATTER.parse( Integer.toString(i+1) + "-04-2018 " + "8:30" );
                Date timeOfArrival =  DATE_TIME_FORMATTER.parse( Integer.toString(i+1) + "-04-2018 " + "10:30" );
                Route r = routes.get(0);
                Train t = new Train(Route.nextId(), timeOfDeparture, timeOfArrival, r);

                routes.get(0).addTrain(t);

                timeOfDeparture =  DATE_TIME_FORMATTER.parse( Integer.toString(i+1) + "-04-2018 " + "15:30" );
                timeOfArrival =  DATE_TIME_FORMATTER.parse( Integer.toString(i+1) + "-04-2018 " + "17:30" );

                Train t2 = new Train(Route.nextId(), timeOfDeparture, timeOfArrival, r);

                routes.get(0).addTrain(t2);

                t.setTrainSet(trainSets.get(i));
                t2.setTrainSet(trainSets.get(i));

                trainSets.get(i).addTrain(t);
                trainSets.get(i).addTrain(t2);

                trains.put(t.getId(), t);
                trains.put(t2.getId(), t2);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    /* Initialization of Vehicles */
    {
        rides = new HashMap<>();
        try {
            Date timeOfDeparture = DATE_TIME_FORMATTER.parse("01-01-1999 " + "23:58");
            Date timeOfArrival = DATE_TIME_FORMATTER.parse("01-01-1999 " + "23:59");
            Route route = new Route(-1, "R000000", depots.get(0), depots.get(1), timeOfDeparture, timeOfArrival);
            RouteSection rs = new RouteSection(-1, route, depots.get(0), depots.get(1), timeOfDeparture, timeOfArrival);
            route.addRouteSection(rs);
            Train t = new Train(-1, timeOfDeparture, timeOfArrival, route);
            Ride ride = new Ride(-1, null, null, t, rs);

            List<Vehicle> vehicles = new ArrayList<>();
            vehicles.addAll(Data.locomotives.values());
            vehicles.addAll(Data.passengerCars.values());
            vehicles.addAll(Data.diningCars.values());
            ride.setVehicles(vehicles);

            for (Car car : Data.passengerCars.values()) {
                car.addRide(ride);
            }

            for (Car car : Data.diningCars.values()) {
                car.addRide(ride);
            }

            for (Locomotive locomotive : Data.locomotives.values()) {
                locomotive.addRide(ride);
            }

            rides.put(ride.getId(), ride);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    /* Creation of Rides */
    {
        rides = new HashMap<>();

        for (int i = 0; i < NUMBER_OF_TRAINS; i++) {
                Train t = trains.get(i);
                List<Vehicle> cars = new ArrayList<>();
                cars.add(passengerCars.get(i));
                cars.add(passengerCars.get(i+1));
                cars.add(diningCars.get(i));
                cars.add(locomotives.get(i));

                for (RouteSection rs : t.getRoute().getRouteSections()) {
                    Ride ride = new Ride(Ride.nextId(), drivers.get(i), cars, t, rs);
                    t.addRide(ride);
                    for (Vehicle car : cars) {
                        car.addRide(ride);
                    }

                    rides.put(ride.getId(), ride);

                }
        }
    }
}

