package org.cz.cvut.fit.rajdlja1.ds.repositoryinterface;


import org.cz.cvut.fit.rajdlja1.ds.domain.TrainSet;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;

import java.util.Date;

/**
 * Created by JR on 3/20/2018.
 */
public interface TrainSetRepositoryInterface extends ObjectRepositoryInterface<TrainSet> {

    public Iterable<TrainSet> findAll(Date since, Date until, StatusEnum status);
}
