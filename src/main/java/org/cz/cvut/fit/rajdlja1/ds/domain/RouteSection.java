package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * Created by JR on 5/18/2018.
 */
@ApiModel(description = "Úsek daného spoje mezi dvěma nádražími.")
public class RouteSection {

    private static int SEQUENCE_ID = 0;
    public static int nextId() {
        return SEQUENCE_ID++;
    }

    @ApiModelProperty(value = "Identifikační číslo jízdy.")
    private Integer id;

    @JsonView()
    private Route route;
    private Depot from;
    private Depot to;

    @ApiModelProperty(value = "Datum a čas odjezdu vlakové soupravy.")
    private Date departureTime;

    @ApiModelProperty(value = "Datum a čas příjezdu vlakové soupravy.")
    private Date arrivalTime;

    public RouteSection() {
    }

    public RouteSection(Integer id) {
        this.id = id;
    }

    public RouteSection(Integer id, Route route, Depot from, Depot to) {
        this.id = id;
        this.route = route;
        this.from = from;
        this.to = to;
    }

    public RouteSection(Integer id, Route route, Depot from, Depot to, Date departureTime, Date arrivalTime) {
        this.id = id;
        this.route = route;
        this.from = from;
        this.to = to;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public Integer getId() {
        return id;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Depot getFrom() {
        return from;
    }

    public void setFrom(Depot from) {
        this.from = from;
    }

    public Depot getTo() {
        return to;
    }

    public void setTo(Depot to) {
        this.to = to;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
