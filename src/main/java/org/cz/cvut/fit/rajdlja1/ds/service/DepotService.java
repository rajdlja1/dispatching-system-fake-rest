package org.cz.cvut.fit.rajdlja1.ds.service;

import org.cz.cvut.fit.rajdlja1.ds.domain.AvailableVehicles;
import org.cz.cvut.fit.rajdlja1.ds.domain.Depot;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.DepotMockedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by JR on 4/6/2018.
 */
@Service
public class DepotService {

    @Autowired
    private DepotMockedRepository repository;

    public Depot findOne(Integer id, Date date) throws NotFoundException {
        return repository.findOne(id, date);
    }

    public AvailableVehicles findAvailableVehicles(Integer id, Date since, Date until) throws NotFoundException {
        return repository.findAvailableVehicles(id, since, until);
    }
}
