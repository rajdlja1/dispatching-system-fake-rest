package org.cz.cvut.fit.rajdlja1.ds.service;

import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.RouteMockedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by JR on 4/5/2018.
 */
@Service
public class RouteService {

    @Autowired
    private RouteMockedRepository repository;

    public Iterable<Route> findAll(Date since, Date until, StatusEnum status) {
        return repository.findAll(since, until, status);
    }


    public Route findOne(Integer id) throws NotFoundException {
        return repository.findOne(id);
    }

    public Route findFilteredOne(Integer id, Date since, Date until, StatusEnum status) throws NotFoundException {
        return repository.findFilteredOne(id, since, until, status);
    }

    @Transactional
    public <S extends Route> S save(S obj) {
        S ts = repository.save(obj);
        return obj;
    }

    @Transactional
    public <S extends Route> S update(S obj) throws NotFoundException {
        if (repository.exists(obj.getId())) {
            S t = repository.update(obj);
            return t;
        } else
            throw new NotFoundException();
    }

    @Transactional
    public void delete(Integer id) throws NotFoundException {
        if (repository.exists(id)) {
            repository.delete(id);
        } else throw new NotFoundException();
    }


}
