package org.cz.cvut.fit.rajdlja1.ds.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.*;
import org.cz.cvut.fit.rajdlja1.ds.domain.ErrorInfo;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;
import org.cz.cvut.fit.rajdlja1.ds.exception.BadFormatException;
import org.cz.cvut.fit.rajdlja1.ds.service.RouteService;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 4/4/2018.
 */

@RestController
@RequestMapping("/spoje")
@Api(description="Operace týkající se obrazovky spojů.")
public class RouteController {

    private static final String ROUTE_BAD_FORMAT = "Spoj neodpovídá požadovanému formátu.";

    @Autowired
    private RouteService service;

    @JsonView(CustomView.RouteView.class)
    @RequestMapping(path="/hledej", method= RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí seznam všech spojů pro zvolený  časový interval.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácený seznam spojů pro interval. " +
                    "Pokud není vyplněn některý z časových údajů od, do, je tento údaj nahrazen aktuálním datem s časem 0:00 pro od a 23:59 pro do." +
                    "Pokud není vyplněn parametr status, defaultně se vyhledávají vlaky, které jsou aktuálně na cestě.") })
    public Iterable<Route> getRoutes (@RequestParam(value = "od", required = false) String strSince,
                                      @RequestParam(value = "do", required = false) String strUntil,
                                      @RequestParam(value = "cas", required = false) String strStatus
    ) throws BadFormatException {
        Iterable<Route> routes = new ArrayList<>();
        List<Date> dates = ControllerValidation.validateDateFromUrl(strSince,strUntil);

        if (strStatus != null) {
            return service.findAll(dates.get(0), dates.get(1), ControllerValidation.validateStatusParameterFromUrl(strStatus));
        }

        return service.findAll(dates.get(0), dates.get(1), StatusEnum.ON_THE_WAY);
    }

    @JsonView(CustomView.RouteDetailView.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí detail spoje podle jeho identifikátoru.", response = Route.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácené detailní informace o spoji."),
            @ApiResponse(code = 404, message = "Spoj se zadaným identifikátorem neexistuje.")
    })
    public Route getRoute(@ApiParam(value = "ID spoje", required = true) @PathVariable("id") Integer id) throws NotFoundException {
        return service.findOne(id);
    }

    @JsonView(CustomView.RouteDetailView.class)
    @RequestMapping(value = "/{id}/hledej", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí vlakovou soupravu podle identifikátoru soupravy v daném časovém intervalu.", response = Route.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácený objekt vlakové soupravy."),
            @ApiResponse(code = 404, message = "Vlaková souprava se zadaným identifikátorem neexistuje.")
    })
    public Route getFilteredRoute(@ApiParam(value = "ID spoje", required = true) @PathVariable("id") Integer id,
                                        @RequestParam(value = "od", required = false) String strSince,
                                        @RequestParam(value = "do", required = false) String strUntil,
                                        @RequestParam(value = "cas", required = false) String strStatus) throws NotFoundException, BadFormatException {
        List<Date> dates = ControllerValidation.validateDateFromUrl(strSince,strUntil);

        if (strStatus != null) {
            return service.findFilteredOne(id, dates.get(0), dates.get(1), ControllerValidation.validateStatusParameterFromUrl(strStatus));
        }

        return service.findFilteredOne(id, dates.get(0), dates.get(1), StatusEnum.ON_THE_WAY);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Smaže spoj se zadaným identifikátorem.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Spoj byl úspěšně smazán."),
            @ApiResponse(code = 404, message = "Spoj se zadaným identifikátorem neexistuje.")
    })
    public void deleteRoute(@PathVariable("id") Integer id) throws NotFoundException {
        service.delete(id);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Vytvoří spoj a vrátí vytvořený objekt v json formátu.", response = Route.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Úspěšně uložený spoj."),
            @ApiResponse(code = 400, message = "Vkládaný objekt není ve správném formátu.")
    })
    @ResponseBody
    public Route createRoute(@RequestBody @Validated Route route) throws BadFormatException {
        if (route == null) throw new BadFormatException(ROUTE_BAD_FORMAT);
        if (route.getId() != null) throw new BadFormatException(ROUTE_BAD_FORMAT);
        return service.save(route);
    }

    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "Upraví existující záznam spoje podle předaného identifikátoru.", response = Route.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Úspěšně uložené změny pro daný spoj."),
            @ApiResponse(code = 400, message = "Objekt představující vlakový spoj není ve správném formátu."),
            @ApiResponse(code = 404, message = "Spoj se zadaným identifikátorem neexistuje.")
    })
    @ResponseBody
    public Route updateRoute(@PathVariable("id") Integer id, @RequestBody Route route) throws BadFormatException, NotFoundException {
        if (route == null) throw new BadFormatException(ROUTE_BAD_FORMAT);
        if (id == null) throw new BadFormatException(ROUTE_BAD_FORMAT);
        return service.update(route);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> rulesForNotFoundException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.NOT_FOUND;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Spoj se zadaným id neexistuje.");
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadFormatException.class)
    public ResponseEntity<ErrorInfo> rulesForBadRequestException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(), e.getMessage());
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<ErrorInfo> rulesForTypeMismatchException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Špatný formát id spoje v url adrese.");
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }
}
