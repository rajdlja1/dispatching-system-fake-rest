package org.cz.cvut.fit.rajdlja1.ds.view;

public class CustomView {
    public static interface OveralView {}
    public static interface DetailView extends OveralView {}
    public static interface ProductView{}
    public static interface TrainsetView{}
    public static interface TrainsetDetailView{}
    public static interface RouteDetailView{}
    public static interface RouteView{}
    public static interface RideView{}
}