package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JR on 3/27/2018.
 */

@ApiModel(description="Model představující depo")
public class Depot implements Serializable {

    @ApiModelProperty(value = "Identifikační číslo depa.")
    private Integer id;

    @ApiModelProperty(value = "Lokace depa.")
    private String location;

    public Depot(Integer id, String location) {
        this.id = id;
        this.location = location;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
