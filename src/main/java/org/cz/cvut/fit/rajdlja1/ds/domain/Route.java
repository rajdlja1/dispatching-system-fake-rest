package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 3/27/2018.
 */
@ApiModel(description="Model představující jeden úsek spoje jízdního řádu")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Route implements Serializable {

    private static int SEQUENCE_ID = 0;

    public static int nextId() {
        return SEQUENCE_ID++;
    }

    public Route(Integer id) {
        this.id = id;
    }

    @ApiModelProperty(value = "Identifikátor spoje.")
    private Integer id;

    @ApiModelProperty(value = "Označení spoje.")
    private String name;

    @ApiModelProperty(value = "Železniční stanice odjezdu vlakové soupravy.")
    private Depot from;

    @ApiModelProperty(value = "Železniční stanice příjezdu vlakové soupravy.")
    private Depot to;

    @ApiModelProperty(value = "Datum a čas odjezdu vlakové soupravy.")
    private Date departureTime;

    @ApiModelProperty(value = "Datum a čas příjezdu vlakové soupravy.")
    private Date arrivalTime;

    @JsonView()
    private List<RouteSection> routeSections;

    @ApiModelProperty(value = "Seznam spojů, které obsluhuje daná vlaková souprava.")
    @JsonView({CustomView.RouteDetailView.class, CustomView.RouteView.class})
    private List<Train> trains;

    public Route(Route another) {
        this.id = another.id;
        this.name = another.name;
        this.from = another.from;
        this.to = another.to;
        this.departureTime = another.departureTime;
        this.arrivalTime = another.arrivalTime;
        this.routeSections = another.routeSections;
        this.trains = another.trains;
    }

    public Route(Integer id, String name, Depot from, Depot to, Date departureTime, Date arrivalTime) {
        this.id = id;
        this.name = name;
        this.from = from;
        this.to = to;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.trains = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Depot getFrom() {
        return from;
    }

    public void setFrom(Depot from) {
        this.from = from;
    }

    public Depot getTo() {
        return to;
    }

    public void setTo(Depot to) {
        this.to = to;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public boolean arrivalBetween(Date since, Date until) {
        if (since.compareTo(this.getArrivalTime()) > 0)
            return false;

        return until.compareTo(this.getArrivalTime()) >= 0;
    }

    public boolean departureBetween(Date since, Date until) {

        if (since.compareTo(this.getDepartureTime()) > 0)
            return false;

        return until.compareTo(this.getDepartureTime()) >= 0;
    }

    public boolean onTheWayBetween(Date since, Date until) {

        if (since.compareTo(this.getDepartureTime()) < 0)
            return false;

        return until.compareTo(this.getArrivalTime()) <= 0;
    }

    public List<RouteSection> getRouteSections() {
        return routeSections;
    }

    public void setRouteSections(List<RouteSection> routeSections) {
        this.routeSections = routeSections;
    }

    public void addRouteSection(RouteSection routeSection) {
        if (routeSections == null) {
            routeSections = new ArrayList<>();
        }
        routeSections.add(routeSection);
    }

    public List<Train> getTrains() {
        return this.trains;
    }

    public void setTrains(List<Train> trains) {
        this.trains = trains;
    }

    public void addTrain(Train train) {
        if (this.trains == null) {
            this.trains = new ArrayList<>();
        }
        this.trains.add(train);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
