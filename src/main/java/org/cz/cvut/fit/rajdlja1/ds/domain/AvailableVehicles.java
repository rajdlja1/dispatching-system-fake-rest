package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

/**
 * Created by JR on 6/6/2018.
 */
public class AvailableVehicles {

    @ApiModelProperty(value = "Depo.")
    private Depot depot;

    @ApiModelProperty(value = "Seznam vlaků a vozů dostupných v depu.")
    private List<Vehicle> vehicles;

    @ApiModelProperty(value = "Datum, od kterého byly vyhledávány dostupné vagony a lokomotivy v depu.")
    private Date since;

    @ApiModelProperty(value = "Datum, po které byly vyhledávány dostupné vagony a lokomotivy v depu.")
    private Date until;

    public AvailableVehicles(Depot depot, Date since, Date until) {
        this.depot = depot;
        this.since = since;
        this.until = until;
    }

    public Depot getDepot() {
        return depot;
    }

    public void setDepot(Depot depot) {
        this.depot = depot;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }
}
