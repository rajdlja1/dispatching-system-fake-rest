package org.cz.cvut.fit.rajdlja1.ds.repositoryinterface;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import java.util.List;

/**
 * Created by JR on 3/20/2018.
 */
public interface ObjectRepositoryInterface<T> {

    T findOne(Integer id) throws NotFoundException;

    List<T> findAll();

    boolean exists(Integer id);

    <S extends T> S save(S obj);

    void delete(Integer id) throws NotFoundException;

    <S extends T> S update(S obj);

}
