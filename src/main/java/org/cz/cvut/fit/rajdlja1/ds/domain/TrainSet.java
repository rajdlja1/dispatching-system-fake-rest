package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JR on 3/19/2018.
 */
@ApiModel(description="Model vlakové soupravy")
public class TrainSet implements Serializable {

    @ApiModelProperty(value = "Identifikátor vlakové soupravy.")
    private Integer id;

    @ApiModelProperty(value = "Pojmenování vlakové soupravy.")
    private String name;

    @ApiModelProperty(value = "Seznam vlaků v dané soupravě.")
    private List<Vehicle> vehicles;

    @ApiModelProperty(value = "Seznam spojů, které obsluhuje daná vlaková souprava.")
    private List<Train> trains;

    public TrainSet(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.trains = new ArrayList<>();
    }

    public TrainSet(TrainSet ts) {
        this.id = ts.id;
        this.name = ts.name;
        this.vehicles = ts.vehicles;
        this.trains = ts.trains;
    }

    public Integer getId() {
        return id;
    }



    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Train> getTrains() {
        return this.trains;
    }

    public void setTrains(List<Train> trains) {
        this.trains = trains;
    }

    public void addTrain(Train train) {
        if (this.trains == null) {
            this.trains = new ArrayList<>();
        }
        this.trains.add(train);
    }

    public void addVehicle(Vehicle vehicle) {
        if (this.vehicles == null) {
            this.vehicles = new ArrayList<>();
        }
        this.vehicles.add(vehicle);
    }

}
