package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JR on 6/25/2018.
 */

@ApiModel(description="Model vozidla (lokomotivy nebo vagonu)")
@JsonDeserialize(as = Car.class)
public abstract class Vehicle implements Serializable {

    public static int SEQUENCE_ID = 0;

    public static int nextId() {
        return SEQUENCE_ID++;
    }

        @ApiModelProperty(value = "Identifikační číslo vozidla.")
        protected Integer id;

        @ApiModelProperty(value = "Jméno vozidla.")
        protected String name;

        @ApiModelProperty(value = "Základní popis vozidla")
        protected String description;

        @JsonView()
        @ApiModelProperty(value = "Seznam defektů dané lokomotivy.")
        protected List<Defect> defects;

        @JsonView()
        @ApiModelProperty(value = "Seznam vlakových souprav, ve kterých je dané vozidlo využito.")
        protected List<TrainSet> trainSets;

        @JsonBackReference
        @ApiModelProperty(value = "Seznam jízd vztahujících se ke konkrétnímu úseku vozidla konkrétního spoje.")
        protected List<Ride> rides;

        public List<Ride> getRides() {
            return rides;
        }

        public void setRides(List<Ride> rides) {
            this.rides = rides;
        }

        public Vehicle(Integer id, String name, String description) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.rides = new ArrayList<>();
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Defect> getDefects() {
            return defects;
        }

        public void setDefects(List<Defect> defects) {
            this.defects = defects;
        }

        public List<TrainSet> getTrainSets() {
            return trainSets;
        }

        public void setTrainSets(List<TrainSet> trainSets) {
            this.trainSets = trainSets;
        }

        public void addRide(Ride r) {
            if (this.rides == null) {
                this.rides = new ArrayList<>();
            }
            this.rides.add(r);
        }
    }

