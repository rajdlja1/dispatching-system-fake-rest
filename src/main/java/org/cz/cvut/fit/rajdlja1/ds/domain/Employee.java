package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by JR on 3/27/2018.
 */
@ApiModel(description="Model představující zaměstnance dopravní společnosti")
public class Employee implements Serializable {

    @ApiModelProperty(value = "Identifikátor zaměstnance.")
    private Integer id;

    @ApiModelProperty(value = "Křestní jméno zaměstnance.")
    private String firstname;

    @ApiModelProperty(value = "Příjmení zamsěstnance.")
    private String lastname;

    @ApiModelProperty(value = "Telefonní číslo zaměstnance.")
    private String phoneNumber;

    @ApiModelProperty(value = "Email zaměstnance.")
    private String email;

    public Employee(Integer id, String firstname, String lastname, String phoneNumber, String email) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
