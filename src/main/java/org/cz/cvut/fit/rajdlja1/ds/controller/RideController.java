package org.cz.cvut.fit.rajdlja1.ds.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.*;
import org.cz.cvut.fit.rajdlja1.ds.domain.ErrorInfo;
import org.cz.cvut.fit.rajdlja1.ds.domain.Ride;
import org.cz.cvut.fit.rajdlja1.ds.exception.BadFormatException;
import org.cz.cvut.fit.rajdlja1.ds.service.RideService;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by JR on 6/28/2018.
 */

@RestController
@RequestMapping("/jizdy")
@Api(description="Operace týkající se jízdy a konfigurace soupravy pro úseky jízdy.")
public class RideController {

    private static final String RIDE_BAD_FORMAT = "Jízda neodpovídá požadovanému formátu.";

    @Autowired
    private RideService service;

    @JsonView(CustomView.RideView.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí detail jízdy podle zadaného identifikátoru.", response = Ride.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácená jízda."),
            @ApiResponse(code = 404, message = "Jízda se zadaným identifikátorem neexistuje.")
    })
    public Ride getRide(@ApiParam(value = "ID jízdy", required = true) @PathVariable("id") Integer id) throws NotFoundException {
        return service.findOne(id);
    }

    @JsonView(CustomView.RideView.class)
    @CrossOrigin
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{id}/konfigurace", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "Upraví existující záznam jízdy a upraví konfiguraci v daném úseku.", response = Ride.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Úspěšně uložené změny pro daný spoj."),
            @ApiResponse(code = 400, message = "Objekt představující jízdu není ve správném formátu."),
            @ApiResponse(code = 404, message = "Jízda se zadaným identifikátorem neexistuje.")
    })
    @ResponseBody
    public Ride updateRide(@PathVariable("id") Integer id, @RequestBody Ride ride) throws BadFormatException, NotFoundException {
        if (ride == null) throw new BadFormatException(RIDE_BAD_FORMAT);
        if (id == null) throw new BadFormatException(RIDE_BAD_FORMAT);
        return service.update(ride);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> rulesForNotFoundException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.NOT_FOUND;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Jízda se zadaným id neexistuje.");
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadFormatException.class)
    public ResponseEntity<ErrorInfo> rulesForBadRequestException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(), e.getMessage());
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<ErrorInfo> rulesForTypeMismatchException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Špatný formát id jízdy v url adrese.");
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }
}
