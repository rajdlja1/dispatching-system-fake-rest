package org.cz.cvut.fit.rajdlja1.ds.service;

import org.cz.cvut.fit.rajdlja1.ds.domain.Ride;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.Data;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.RideMockedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.NotFoundException;

/**
 * Created by JR on 6/28/2018.
 */
@Service
public class RideService {

    @Autowired
    private RideMockedRepository repository;

    public Ride findOne(Integer id) throws NotFoundException {
        return repository.findOne(id);
    }

    @Transactional
    public <S extends Ride> S update(S obj) throws NotFoundException {
        if (repository.exists(obj.getId())) {
            S t = repository.update(obj);
            return t;
        } else
            throw new NotFoundException();
    }
}
