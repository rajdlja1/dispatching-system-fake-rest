package org.cz.cvut.fit.rajdlja1.ds.fakerepository;

import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.cz.cvut.fit.rajdlja1.ds.repositoryinterface.TrainSetRepositoryInterface;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by JR on 3/20/2018.
 */

@Repository
public class TrainSetMockedRepository implements TrainSetRepositoryInterface {

    private static final String TRAINSET_DATA_INTEGRATION_ERROR_MESSAGE = "Vlaková souprava s tímto názvem již existuje.";

    public List<Train> findTrains(Integer id) throws NotFoundException {
        if (!Data.trainSets.containsKey(id)) {
            throw new NotFoundException();
        }

        List<Train> suitableTrains = new ArrayList<>();

        Data.trains.forEach((kr,vr) -> {
            if (vr.getTrainSet().getId() == id) {
                suitableTrains.add(vr);
            }
        });

        return suitableTrains;
    }

    @Override
    public TrainSet findOne(Integer id) throws NotFoundException {
        if (!Data.trainSets.containsKey(id)) {
            throw new NotFoundException();
        }
        return Data.trainSets.get(id);
    }

    public TrainSet findFilteredOne(Integer id, Date since, Date until, StatusEnum status) throws NotFoundException {
        if (!Data.trainSets.containsKey(id)) {
            throw new NotFoundException();
        }

        TrainSet trainset = new TrainSet(Data.trainSets.get(id));
        List<Train> trains = trainset.getTrains();
        trainset.setTrains(new ArrayList<>());
        for (Train train : trains) {
            if (train.isSuitableTrain(status, since, until)) {
                trainset.addTrain(train);
            }
        }
        return trainset;
    }

    @Override
    public List<TrainSet> findAll() {
        List<TrainSet> list = new ArrayList<>(Data.trainSets.values());
        return list;
    }

    public List<TrainSet> findAll(Date since, Date until, StatusEnum status) {
        List<TrainSet> suitableTrainsets = new ArrayList<>();

        Data.trainSets.forEach((kts,vts) -> {
            if (hasSuitableTrain(vts.getTrains(), status, since, until)) {
                suitableTrainsets.add(vts);
            }
        });

            return suitableTrainsets;
    }

    public Boolean hasSuitableTrain(List<Train> trains, StatusEnum status, Date since, Date until) {
        for (Train t: trains) {
            if (t.isSuitableTrain(status, since, until)) {
                return true;
            };
        }
        return false;
    }

    @Override
    public boolean exists(Integer id) {
        if (!Data.trainSets.containsKey(id)) {
            return false;
        }
        return true;
    }

    @Override
    public <S extends TrainSet> S save(S obj) throws DataIntegrityViolationException {

        /* Attribute Name has to be unique */
        Data.trainSets.forEach((k,v) -> {
            if (v.getName().equals(obj.getName())) {
                throw new DataIntegrityViolationException(TRAINSET_DATA_INTEGRATION_ERROR_MESSAGE);
            }
        });

        Integer maxKey = Collections.max(Data.trainSets.keySet());
        obj.setId(maxKey+1);
        Data.trainSets.put(maxKey+1, obj);
        return obj;
    }

    @Override
    public void delete(Integer id) throws NotFoundException {
        if (!Data.trainSets.containsKey(id)) {
            throw new NotFoundException();
        }
        Data.trainSets.remove(id);
    }

    @Override
    public <S extends TrainSet> S update(S obj) {

        /* Attribute Name has to be unique */
        Data.trainSets.forEach((k,v) -> {
            if (v.getName().equals(obj.getName()) && v.getId() != obj.getId() ) {
                throw new DataIntegrityViolationException(TRAINSET_DATA_INTEGRATION_ERROR_MESSAGE);
            }
        });

        S ts = (S)Data.trainSets.get(obj.getId());
        //ts.setVehicles(obj.getVehicles());
        //ts.setVehicles(obj.getVehicles());
        ts.setName(obj.getName());
        //ts.setTrains(obj.getTrains());

        return ts;
    }
}
