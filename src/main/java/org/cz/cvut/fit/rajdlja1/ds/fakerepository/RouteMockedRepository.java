package org.cz.cvut.fit.rajdlja1.ds.fakerepository;

import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;
import org.cz.cvut.fit.rajdlja1.ds.domain.Train;
import org.cz.cvut.fit.rajdlja1.ds.repositoryinterface.RouteRepositoryInterface;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by JR on 4/5/2018.
 */
@Repository
public class RouteMockedRepository implements RouteRepositoryInterface {

    @Override
    public Route findOne(Integer id) throws NotFoundException {
        if (!Data.routes.containsKey(id)) {
            throw new NotFoundException();
        }

        return Data.routes.get(id);
    }

    @Override
    public List<Route> findAll() {
        List<Route> routeList = new ArrayList<>(Data.routes.values());
        return routeList;
    }

    @Override
    public List<Route> findAll(Date since, Date until, StatusEnum status) {
        List<Route> suitableRoutes = new ArrayList<>();

        Data.routes.forEach((kts,r) -> {
            switch (status) {
                case ARRIVAL:
                    if (r.arrivalBetween(since, until)) {
                        suitableRoutes.add(r);
                    }
                    break;
                case DEPARTURE:
                    if (r.departureBetween(since, until)) {
                        suitableRoutes.add(r);
                    }
                    break;
                case ON_THE_WAY:
                    if (r.onTheWayBetween(since, until)) {
                        suitableRoutes.add(r);
                    }
                    break;
                default:
                    break;
            }
        });
        return suitableRoutes;
    }

    @Override
    public boolean exists(Integer id) {
        return Data.routes.containsKey(id);
    }

    @Override
    public <S extends Route> S save(S obj) {
        Integer maxKey = Collections.max(Data.routes.keySet());
        obj.setId(maxKey+1);
        Data.routes.put(maxKey+1, obj);
        return obj;
    }

    @Override
    public void delete(Integer id) throws NotFoundException {
        if (!Data.routes.containsKey(id)) {
            throw new NotFoundException();
        }
        Data.routes.remove(id);
    }

    @Override
    public <S extends Route> S update(S obj) {
        S r = (S)Data.routes.get(obj.getId());
        r.setArrivalTime(obj.getDepartureTime());
        r.setDepartureTime(obj.getDepartureTime());
        r.setFrom(obj.getFrom());
        r.setTo(obj.getTo());

        return r;
    }

    public Route findFilteredOne(Integer id, Date since, Date until, StatusEnum status) throws NotFoundException {
        if (!Data.routes.containsKey(id)) {
            throw new NotFoundException();
        }

        Route route = new Route(Data.routes.get(id));
        List<Train> trains = route.getTrains();
        route.setTrains(new ArrayList<>());
        for (Train train : trains) {
            if (train.isSuitableTrain(status, since, until)) {
                route.addTrain(train);
            }
        }
        return route;
    }




}
