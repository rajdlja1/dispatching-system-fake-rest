package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JR on 3/27/2018.
 */
@ApiModel(description="Model lokomotivy")
public class Locomotive extends Vehicle {

    public Locomotive(Integer id, String name, String description) {
        super(id, name, description);
    }
}
