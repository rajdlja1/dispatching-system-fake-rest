package org.cz.cvut.fit.rajdlja1.ds.service;

import org.cz.cvut.fit.rajdlja1.ds.domain.Train;
import org.cz.cvut.fit.rajdlja1.ds.domain.TrainSet;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;
import org.cz.cvut.fit.rajdlja1.ds.fakerepository.TrainSetMockedRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by JR on 3/20/2018.
 */
@Service
public class TrainSetService {

    @Autowired
    private TrainSetMockedRepository repository;

    public Iterable<TrainSet> findAll(Date since, Date until, StatusEnum status) {
        return repository.findAll(since, until, status);
    }

    public TrainSet findOne(Integer id) throws NotFoundException {
        return repository.findOne(id);
    }

    public TrainSet findFilteredOne(Integer id, Date since, Date until, StatusEnum status) throws NotFoundException {
        return repository.findFilteredOne(id, since, until, status);
    }

    public Iterable<Train> findTrains(Integer id) throws NotFoundException {
        return repository.findTrains(id);
    }

    @Transactional
    public <S extends TrainSet> S save(S obj) {
        S ts = repository.save(obj);
        return obj;
    }

    @Transactional
    public <S extends TrainSet> S update(S obj) throws NotFoundException {
        if (repository.exists(obj.getId())) {
            S t = repository.update(obj);
            return t;
        } else
            throw new NotFoundException();
    }

    @Transactional
    public void delete(Integer id) throws NotFoundException {
        if (repository.exists(id)) {
            repository.delete(id);
        } else throw new NotFoundException();
    }

}
