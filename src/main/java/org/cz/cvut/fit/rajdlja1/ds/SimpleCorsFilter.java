package org.cz.cvut.fit.rajdlja1.ds;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class SimpleCorsFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request,
                         ServletResponse _response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse)_response;
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Headers",
				"Content-Type, Accept, Authorization, X-Requested-With");
		response.addHeader("Access-Control-Allow-Methods", 
				"POST,GET,OPTIONS,DELETE,PUT");
		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {	}
	
	@Override
	public void init(FilterConfig arg0) throws ServletException {}
	
}