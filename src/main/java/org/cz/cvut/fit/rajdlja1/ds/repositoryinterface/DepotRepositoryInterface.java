package org.cz.cvut.fit.rajdlja1.ds.repositoryinterface;

import org.cz.cvut.fit.rajdlja1.ds.domain.Depot;
import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import java.util.Date;

/**
 * Created by JR on 4/6/2018.
 */
public interface DepotRepositoryInterface extends ObjectRepositoryInterface<Depot> {

    Depot findOne(Integer id, Date date) throws NotFoundException;
}
