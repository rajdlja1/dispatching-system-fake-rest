package org.cz.cvut.fit.rajdlja1.ds.domain;

import java.sql.Timestamp;

/**
 * The class used for sending the error message as the response body of the response for CRUD requests.
 * Created by JR on 3/25/2018.
 */

public class ErrorInfo {

    private Timestamp timestamp;
    private int status;
    private String error;
    private String path;
    private String message;

    public ErrorInfo(Timestamp timestamp, int status, String error, String path, String message) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.path = path;
        this.message = message;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
