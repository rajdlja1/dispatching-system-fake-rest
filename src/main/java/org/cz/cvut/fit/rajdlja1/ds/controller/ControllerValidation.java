package org.cz.cvut.fit.rajdlja1.ds.controller;

import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;
import org.cz.cvut.fit.rajdlja1.ds.exception.BadFormatException;

import javax.ws.rs.InternalServerErrorException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 4/5/2018.
 */
public class ControllerValidation {

    private static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat DATE_TIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-h:m");
    private static final String DATE_BAD_FORMAT = "Datum v url adrese musí splňovat následující formát yyyy-MM-dd-h:m, např.: 2018-04-01-9:30.";
    private static final String INTERNAL_DATE_ERROR = "Došlo k interní chybě serveru a požadavek nemohl být zpracován.";
    private static final String BAD_INTERVAL_ERROR = "Zadaný interval je neplatný.";
    private static final String STATUS_BAD_FORMAT = "Parametr Čas v url má povolené pouze tyto hodnoty: PRIJEZD, ODJEZD, NACESTE.";
    private static final Long MILISECONDS_OF_DATE_23_59 = 86340000L;

    public static Date validateDateFromUrl(String strDate) throws BadFormatException {
        Date date = null;
        try {
            date = DATE_TIME_FORMATTER.parse(strDate);
        } catch (ParseException e) {
            throw new BadFormatException(DATE_BAD_FORMAT);
        }
        return date;
    }

    public static List<Date> validateDateFromUrl(String strSince, String strUntil) throws BadFormatException {
        Date dateSince = null;
        Date dateUntil = null;

        try {
            if (strSince == null) {
                dateSince = DATE_FORMATTER.parse(DATE_FORMATTER.format(new Date()));
            } else {
                dateSince = validateDateFromUrl(strSince);
            }

            if (strUntil == null) {
                dateUntil = DATE_FORMATTER.parse(DATE_FORMATTER.format(new Date()));
                dateUntil.setTime(dateUntil.getTime() + MILISECONDS_OF_DATE_23_59);
            } else {
                dateUntil = validateDateFromUrl(strUntil);
            }

        } catch (ParseException e) {
            throw new InternalServerErrorException(INTERNAL_DATE_ERROR);
        }

        if (dateSince.after(dateUntil)) {
            throw new BadFormatException(BAD_INTERVAL_ERROR);
        }

        List<Date> dates = new ArrayList();
        dates.add(dateSince);
        dates.add(dateUntil);
        return dates;
    }

    public static StatusEnum validateStatusParameterFromUrl(String strStatus) throws BadFormatException {
        String upperStatus = strStatus.toUpperCase();

        for (StatusEnum status : StatusEnum.values()) {
            if (upperStatus.equals(status.getCsTranslation())) {
                return status;
            }
        }

        throw new BadFormatException(STATUS_BAD_FORMAT);
    }

}
