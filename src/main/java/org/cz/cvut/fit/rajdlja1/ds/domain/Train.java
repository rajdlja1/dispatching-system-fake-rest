package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiModelProperty;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 5/19/2018.
 */
public class Train {

    private static int SEQUENCE_ID = 0;

    public static int nextId() {
        return SEQUENCE_ID++;
    }

    @ApiModelProperty(value = "Identifikační číslo jízdy.")
    private Integer id;

    @JsonView({CustomView.TrainsetView.class, CustomView.TrainsetDetailView.class})
    @ApiModelProperty(value = "Spoj, ke kterému se tento vlak vztahuje.")
    private Route route;

    @ApiModelProperty(value = "Strojvedoucí vlakové soupravy na daném úseku.")
    private Driver driver;

    @ApiModelProperty(value = "Datum a čas odjezdu vlakové soupravy.")
    private Date departureTime;

    @ApiModelProperty(value = "Datum a čas příjezdu vlakové soupravy.")
    private Date arrivalTime;

    @JsonView()
    @ApiModelProperty(value = "Vlaková souprava obsluhující daný vlak.")
    private TrainSet trainSet;

    @ApiModelProperty(value = "Seznam jízd úseků, které jsou na trase daného vlaku.")
    private List<Ride> rides;

    public Train(Integer id, Date departureTime, Date arrivalTime) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public Train(Integer id, Date departureTime, Date arrivalTime, Route route) {
        this.id = id;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.route = route;
    }

    public Train() {
    }

    public Train(Train another) {
        this.id = another.id;
        this.driver = another.driver;
        this.departureTime = another.departureTime;
        this.arrivalTime = another.arrivalTime;
        this.trainSet = another.trainSet;
        this.rides = another.rides;
    }

    public Train(Integer id, Driver driver, Depot from, Depot to, Date departureTime, Date arrivalTime, TrainSet trainSet, List<Ride> routeSections) {
        this.id = id;
        this.driver = driver;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.trainSet = trainSet;
        this.rides = routeSections;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TrainSet getTrainSet() {
        return trainSet;
    }

    public void setTrainSet(TrainSet trainSet) {
        this.trainSet = trainSet;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public boolean arrivalBetween(Date since, Date until) {
        if (since.compareTo(this.getArrivalTime()) > 0)
            return false;

        return until.compareTo(this.getArrivalTime()) >= 0;
    }

    public boolean departureBetween(Date since, Date until) {

        if (since.compareTo(this.getDepartureTime()) > 0)
            return false;

        return until.compareTo(this.getDepartureTime()) >= 0;
    }

    public boolean onTheWayBetween(Date since, Date until) {

        if (since.compareTo(this.getDepartureTime()) < 0)
            return false;

        return until.compareTo(this.getArrivalTime()) <= 0;
    }

    public Route getRoute() {
        return route;
    }

    public List<Ride> getRides() {
        return rides;
    }

    public void addRide(Ride ride) {
        if (this.rides == null) {
            rides = new ArrayList<>();
        }
        rides.add(ride);
    }

    public Boolean isSuitableTrain(StatusEnum status, Date since, Date until) {
        switch (status) {
            case ARRIVAL:
                return arrivalBetween(since, until);
            case DEPARTURE:
                return departureBetween(since, until);
            case ON_THE_WAY:
                return onTheWayBetween(since, until);
            default:
                return false;
        }
    }

}
