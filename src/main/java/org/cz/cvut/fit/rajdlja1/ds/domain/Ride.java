package org.cz.cvut.fit.rajdlja1.ds.domain;

import com.fasterxml.jackson.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.cz.cvut.fit.rajdlja1.ds.view.CustomView;

import java.io.Serializable;
import java.util.List;

/**
 * Created by JR on 5/18/2018.
 */

@ApiModel(description="Model představující jízdy na jednom úseku konkrétního vlaku")
public class Ride implements Serializable {

    private static int SEQUENCE_ID = 0;

    public static int nextId() {
        return SEQUENCE_ID++;
    }

    @ApiModelProperty(value = "Identifikační číslo jízdy.")
    private Integer id;

    @ApiModelProperty(value = "Označení spoje.")
    private Integer name;

    @ApiModelProperty(value = "Řidič soupravy v daném úseku.")
    private Driver driver;

    @ApiModelProperty(value = "Seznam vagónů a lokomotiv, které jsou zapojeny v dané soupravě.")
    private List<Vehicle> vehicles;

    @ApiModelProperty(value = "Vlak konkrétního spoje, ke kterému se jízda vztahuje.")
    @JsonBackReference
    private Train train;

    @JsonView({CustomView.RouteDetailView.class, CustomView.TrainsetDetailView.class, CustomView.RideView.class})
    @ApiModelProperty(value = "Úsek dané jízdy daného vlaku.")
    private RouteSection routeSection;

    public Ride(Integer id) {
        this.id = id;
    }

    public Ride(Integer id, Driver driver, List<Vehicle> vehicles, Train train, RouteSection routeSection) {
        this.id = id;
        this.driver = driver;
        this.vehicles = vehicles;
        this.train = train;
        this.routeSection = routeSection;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public RouteSection getRouteSection() {
        return routeSection;
    }

    public void setRouteSection(RouteSection routeSection) {
        this.routeSection = routeSection;
    }

    public Integer getId() {
        return id;
    }


}
