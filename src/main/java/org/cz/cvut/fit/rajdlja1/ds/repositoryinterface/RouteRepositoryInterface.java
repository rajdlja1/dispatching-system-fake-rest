package org.cz.cvut.fit.rajdlja1.ds.repositoryinterface;

import org.cz.cvut.fit.rajdlja1.ds.domain.Route;
import org.cz.cvut.fit.rajdlja1.ds.domain.StatusEnum;

import java.util.Date;

/**
 * Created by JR on 4/5/2018.
 */
public interface RouteRepositoryInterface extends ObjectRepositoryInterface<Route> {

    Iterable<Route> findAll(Date since, Date until, StatusEnum status);

}
