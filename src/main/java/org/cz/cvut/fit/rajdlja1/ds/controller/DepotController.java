package org.cz.cvut.fit.rajdlja1.ds.controller;

import io.swagger.annotations.*;
import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.cz.cvut.fit.rajdlja1.ds.exception.BadFormatException;
import org.cz.cvut.fit.rajdlja1.ds.service.DepotService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 4/6/2018.
 */
@RestController
@RequestMapping("/depa")
@Api(description="Operace pro správu dostupnosti vozů a vlaků v depu.")
public class DepotController {

    @Autowired
    private DepotService service;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí detail vybraného depa podle jeho identifikátoru k dnešnímu dni. Při zadání jiného data se zobrazí detail depa k tomuto datu.", response = Depot.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácené detailní informace o depu, jeho vozech a vlacích."),
            @ApiResponse(code = 404, message = "Depo se zadaným identifikátorem neexistuje.")
    })
    public Depot getDepot(@ApiParam(value = "ID depa", required = true) @PathVariable("id") Integer id,
                          @RequestParam(value = "datum", required = false) String strDate) throws NotFoundException, BadFormatException {

        if (strDate != null) {
            return service.findOne(id, ControllerValidation.validateDateFromUrl(strDate));
        }

        Date now = DateTime.now().toDate();
        return service.findOne(id, now);
    }

    @RequestMapping(value = "/{id}/dostupnost", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Zobrazí dostupné vlaky a vagony v depu pro zvolený časový interval.", response = AvailableVehicles.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Úspěšně vrácené detailní informace o depu, jeho vozech a vlacích."),
            @ApiResponse(code = 404, message = "Depo se zadaným identifikátorem neexistuje.")
    })
    public AvailableVehicles getAvailableVehiclesOfDepot(@ApiParam(value = "ID depa", required = true) @PathVariable("id") Integer id,
                                              @RequestParam(value = "od", required = false) String strSince,
                                              @RequestParam(value = "do", required = false) String strUntil) throws NotFoundException, BadFormatException {
        List<Date> dates = ControllerValidation.validateDateFromUrl(strSince,strUntil);
        return service.findAvailableVehicles(id, dates.get(0), dates.get(1));
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> rulesForNotFoundException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.NOT_FOUND;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(),"Depo se zadaným id neexistuje.");
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadFormatException.class)
    public ResponseEntity<ErrorInfo> rulesForBadRequestException(HttpServletRequest req, Exception e) {
        HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
        Date date = new Date();
        ErrorInfo errInfo = new ErrorInfo(new Timestamp(date.getTime()), responseStatus.value(), e.getMessage(),  req.getRequestURI(), e.getMessage());
        return new ResponseEntity<ErrorInfo>(errInfo, responseStatus);
    }

}
