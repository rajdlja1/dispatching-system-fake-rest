package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by JR on 3/27/2018.
 */
@ApiModel(description="Strojvedoucí vlakové soupravy.")
public class Driver extends Employee implements Serializable {

    @ApiModelProperty(value = "Seznam spojů, pro které je obsazen jako strojvedoucí vlakové soupravy.")
    private List<Route> routes;

    public Driver(Integer id, String firstname, String lastname, String phoneNumber, String email) {
        super(id, firstname, lastname, phoneNumber, email);
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
