package org.cz.cvut.fit.rajdlja1.ds.comparator;

import java.util.Calendar;
import java.util.Comparator;

/**
 * Created by JR on 4/4/2018.
 */
public class TimeIgnoringComparator implements Comparator<Calendar> {
    public int compare(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
        return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
    }
}
