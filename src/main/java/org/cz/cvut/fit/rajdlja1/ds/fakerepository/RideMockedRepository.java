package org.cz.cvut.fit.rajdlja1.ds.fakerepository;

import org.cz.cvut.fit.rajdlja1.ds.domain.Ride;
import org.springframework.stereotype.Repository;

import javax.ws.rs.NotFoundException;


/**
 * Created by JR on 6/28/2018.
 */
@Repository
public class RideMockedRepository {

    public boolean exists(Integer id) {
        return Data.rides.containsKey(id);
    }

    public Ride findOne(Integer id) throws NotFoundException {
        if (!Data.rides.containsKey(id)) {
            throw new NotFoundException();
        }

        return Data.rides.get(id);
    }

    public <S extends Ride> S update(S obj) {
        S r = (S)Data.rides.get(obj.getId());
        r.setVehicles(obj.getVehicles());

        /*only change of the configuration*/
        //r.setTrain(obj.getTrain());
        //r.setDriver(obj.getDriver());
        //r.setRouteSection(obj.getRouteSection());

        return r;
    }

}
