package org.cz.cvut.fit.rajdlja1.ds.fakerepository;

import org.cz.cvut.fit.rajdlja1.ds.domain.*;
import org.cz.cvut.fit.rajdlja1.ds.repositoryinterface.DepotRepositoryInterface;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by JR on 4/6/2018.
 */
@Repository
public class DepotMockedRepository implements DepotRepositoryInterface {


    @Override
    public Depot findOne(Integer id, Date date) throws NotFoundException {
        if (!Data.depots.containsKey(id)) {
            throw new NotFoundException();
        }

        return Data.depots.get(id);
    }

    public AvailableVehicles findAvailableVehicles(Integer id, Date since, Date until) throws NotFoundException {

        List<Vehicle> availableVehicles = new ArrayList<>();

        List<Vehicle> availableLocomotives = findAvailableVehicles(id, since, until, new ArrayList<>(Data.locomotives.values()));
        availableVehicles.addAll(availableLocomotives);
        List<Vehicle> availableDiningVehicles = findAvailableVehicles(id, since, until, new ArrayList<>(Data.diningCars.values()));
        availableVehicles.addAll(availableDiningVehicles);
        List<Vehicle> availablePassengerVehicles = findAvailableVehicles(id, since, until, new ArrayList<>(Data.passengerCars.values()));
        availableVehicles.addAll(availablePassengerVehicles);

        AvailableVehicles aVehicles = new AvailableVehicles(findOne(id), since, until);
        aVehicles.setVehicles(availableVehicles);

        return aVehicles;
    }

    public List<Vehicle> findAvailableVehicles(Integer id, Date since, Date until, Collection<Vehicle> vehicleList) throws NotFoundException {
        //TODO change the algorithm

        List<Vehicle> vehicles = new ArrayList();

        for (Vehicle vehicle : vehicleList) {
            Ride lastBeforeAvailabInt = null;
            Ride firstAfterSince = null;

            for (Ride r : vehicle.getRides()) {

                // only rides with departure before the given interval
                // departure time of the ride has to be before the since parameter time
                if (r.getRouteSection().getDepartureTime().compareTo(since) < 0) {
                    if (lastBeforeAvailabInt == null ||
                            (lastBeforeAvailabInt.getRouteSection().getDepartureTime().compareTo(r.getRouteSection().getDepartureTime()) < 0)) {
                        lastBeforeAvailabInt = r;
                    }
                }
                else {
                    // only rides with departure after the SINCE parameter of the given interval
                    if (firstAfterSince == null ||
                            (firstAfterSince.getRouteSection().getDepartureTime().compareTo(r.getRouteSection().getDepartureTime()) > 0)) {
                        firstAfterSince = r;
                    }
                }
            }

            // if the arrival time of the ride is after since parameter - it means that the vehicle is on the way and is not available
            // only the other case is handled - arrival time is before since parameter
            if (lastBeforeAvailabInt != null && (since.compareTo(lastBeforeAvailabInt.getRouteSection().getArrivalTime()) > 0)) {
                if (lastBeforeAvailabInt.getRouteSection().getTo().getId() == id) {
                    // the vehicle is in the correct depot available in the time of SINCE parameter
                    if (firstAfterSince == null || (firstAfterSince.getRouteSection().getDepartureTime().compareTo(until) > 0)) {
                        vehicles.add(vehicle);
                    }
                }
            }
        }

        return vehicles;
    }


    @Override
    public Depot findOne(Integer id) throws NotFoundException {
        return null;
    }

    @Override
    public List<Depot> findAll() {
        return null;
    }

    @Override
    public boolean exists(Integer id) {
        return false;
    }

    @Override
    public <S extends Depot> S save(S obj) {
        return null;
    }

    @Override
    public void delete(Integer id) throws NotFoundException {

    }

    @Override
    public <S extends Depot> S update(S obj) {
        return null;
    }
}
