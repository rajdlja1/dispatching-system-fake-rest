package org.cz.cvut.fit.rajdlja1.ds.domain;

/**
 * Created by JR on 4/6/2018.
 */
public enum TrainSetStatusEnum {
    ARRIVAL("PRIJEZD"),
    ON_THE_WAY("NACESTE"),
    DEPARTURE("ODJEZD");

    private String csTranslation;

    private TrainSetStatusEnum(String csTranslation) {
        this.csTranslation = csTranslation;
    }

    public String getCsTranslation() {
        return csTranslation;
    }
}
