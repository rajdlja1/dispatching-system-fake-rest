package org.cz.cvut.fit.rajdlja1.ds.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by JR on 3/27/2018.
 */
@ApiModel(description="Model představující poruchu. Může se jednat buď o poruchu vagónu nebo lokomotivy.")
public class Defect implements Serializable {

    @ApiModelProperty(value = "Identifikační číslo závady.")
    private Integer id;

    @ApiModelProperty(value = "Datum, kdy byla závada registrována.")
    private Date from;

    @ApiModelProperty(value = "Datum, kdy byla závada opravena. U závad, které nejsou dosud upraveny, tato hodnota není vyplněna.")
    private Date to;

    @ApiModelProperty(value = "Popis závady.")
    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
